# crawler

Building the application
---

* Clone this repo.
* Run `mvn clean install` to build your application

Running the crawl command
---

To crawl and parse a URL, run with the following command line arguments:

* `java -jar target/crawler-1.0-SNAPSHOT.jar crawl --url http://someurl/index.html config.yml`

To parse a data file created from a previous run:

* `java -jar target/crawler-1.0-SNAPSHOT.jar crawl --data-file data.json config.yml`

Search terms are optional, but if they appear they must be the first argument:

* `java -jar target/crawler-1.0-SNAPSHOT.jar crawl --terms foo bar baz --url http://someurl/index.html config.yml`
* `java -jar target/crawler-1.0-SNAPSHOT.jar crawl --terms foo bar baz --data-file data.json config.yml`


Crawler Output
---

The `crawl` command will output two JSON files in the working directory.

* data.json - The raw crawl data.
* report.json - the parsed data.

Implemented Features
---

- [x] Allow user to provide URL to be crawled and processed.
- [x] Allow user to provide data file from a previous run to be processed.
- [x] Extract all links that link to outside the domain.
- [x] Extract all email addresses (links and plain text).
- [x] Remove all HTML tags from the body for the report.
- [x] Extract and count terms specified by the user (should also be part of report if 0 are found).

Known Issues
---

* Parsing of URLs and Emails out of the body of the response will require more tweaking to get right.

Technology and Component Design Choices
---

* I initially chose Dropwizard because it provided a way to bootstrap a project quickly with command-line support. In hindsight, it wasn't really necessary and I would probably not use it for this prototype if I were to do it over.
* I tried to code to interfaces where possible.
* Separation of concerns:
  * "crawl" logic represented by the Crawler interface
  * "processor" logic represented by the Processor interface.
  * Report building logic represented by  Reporter interface.
  * File storage in the "CrawlDataStore" interface.




