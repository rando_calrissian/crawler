package org.dudesoft.model;

import org.apache.commons.lang3.Validate;

import java.util.Date;
import java.util.Map;

public class CrawlData {
  private String url;
  private Date crawlDate;
  private Map<String, String> headers;
  private String title;
  private String body;

  // Needed for JSON deserialization
  public CrawlData() {}

  public CrawlData(String url, Date crawlDate) {
    Validate.notEmpty(url, "url parameter is empty");
    Validate.notNull(crawlDate, "crawlDate parameter is empty");
    this.url = url;
    this.crawlDate = crawlDate;
  }

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public Date getCrawlDate() {
    return crawlDate;
  }

  public void setCrawlDate(Date crawlDate) {
    this.crawlDate = crawlDate;
  }

  public Map<String, String> getHeaders() {
    return headers;
  }

  public void setHeaders(Map<String, String> headers) {
    this.headers = headers;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getBody() {
    return body;
  }

  public void setBody(String body) {
    this.body = body;
  }
}
