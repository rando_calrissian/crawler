package org.dudesoft.model;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Class representing the final result.
 */
public class Report extends CrawlData {

  private List<String> outlinks;
  private List<String> emails;
  private List<Map<String, Integer>> custom;

  public Report(String url, Date crawlDate) {
    super(url, crawlDate);
  }

  public List<String> getOutlinks() {
    return outlinks;
  }

  public void setOutlinks(List<String> outlinks) {
    this.outlinks = outlinks;
  }

  public List<String> getEmails() {
    return emails;
  }

  public void setEmails(List<String> emails) {
    this.emails = emails;
  }

  public List<Map<String, Integer>> getCustom() {
    return custom;
  }

  public void setCustom(List<Map<String, Integer>> custom) {
    this.custom = custom;
  }

}
