package org.dudesoft.cli;

import io.dropwizard.cli.ConfiguredCommand;
import io.dropwizard.setup.Bootstrap;
import net.sourceforge.argparse4j.inf.Namespace;
import net.sourceforge.argparse4j.inf.Subparser;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.UrlValidator;
import org.dudesoft.crawlerConfiguration;
import org.dudesoft.model.CrawlData;
import org.dudesoft.model.Report;
import org.dudesoft.services.Crawler;
import org.dudesoft.services.ReportService;
import org.dudesoft.services.Reporter;
import org.dudesoft.services.SimpleCrawlService;
import org.dudesoft.services.SimpleProcessorService;
import org.dudesoft.store.CrawlDataStore;
import org.dudesoft.store.JsonCrawlDataStore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Optional;

public class CrawlCommand extends ConfiguredCommand<crawlerConfiguration> {

  private static final Logger console = LoggerFactory.getLogger("consolelogger");
  private static final Logger log = LoggerFactory.getLogger(CrawlCommand.class);

  private Crawler crawler;
  private Reporter reporter;

  public CrawlCommand() {
    super("crawl", "crawl the provided URL and parse any external links.");
    crawler = new SimpleCrawlService(new JsonCrawlDataStore());
    reporter = new ReportService(new SimpleProcessorService());
  }

  @Override
  public void configure(Subparser subparser) {
    super.configure(subparser);

    subparser.addArgument("-t", "--terms")
        .dest("terms")
        .nargs("+")
        .help("Optional terms to be searched for within URL body");

    subparser.addArgument("-u", "--url")
        .dest("url")
        .type(String.class)
        .help("Required if \"data-file\" argument is not provided. URL to retrieved and parsed.");

    subparser.addArgument("-dt", "--data-file")
        .dest("data-file")
        .type(String.class)
        .help("Required if \"url\" argument is not provided. The path to a data.json file from a previous run");
  }

  @Override
  protected void run(Bootstrap<crawlerConfiguration> bootstrap, Namespace namespace, crawlerConfiguration crawlerConfiguration) throws Exception {

    // verify one of url or data-file is provided
    String url = namespace.getString("url");
    String dataFilePath = namespace.getString("data-file");
    if ((StringUtils.isEmpty(url) && StringUtils.isEmpty(dataFilePath)) ||
        (StringUtils.isNotEmpty(url) && StringUtils.isNotEmpty(dataFilePath))) {
        console.error("One of \"url\" or \"data-file\" is required, but not both.");
        return;
    }

    // if data-file was not provided, crawl first
    if (StringUtils.isEmpty(dataFilePath)) {
      // verify URL is valid before proceeding
      String[] schemes = { "http", "https" };
      UrlValidator urlValidator = new UrlValidator(schemes);
      if (!urlValidator.isValid(url)) {
        console.error("URL is invalid: {}", url);
        return;
      }

      // Retrieve URL response to file
      dataFilePath = "data.json";
      try {
        crawler.downloadToDataFile(url, dataFilePath);
      } catch (Exception e) {
        console.error("Unable to create data file from URL: {}", e.getMessage());
        log.error("download to file failed", e);
        return;
      }

      console.info("Data file written to: {}", new File(dataFilePath).getAbsolutePath());
    }

    // Read crawl data from file
    CrawlData data = crawler.readFromFile(dataFilePath);
    // Get search terms if they were provided
    List<String> terms = namespace.getList("terms");
    Report report = reporter.buildReport(data, Optional.ofNullable(terms));

    // write report file
    CrawlDataStore dataStore = new JsonCrawlDataStore();
    dataStore.save(new FileOutputStream("report.json"), report);

    console.info("Report file written to: {}", new File("report.json").getAbsolutePath());
  }
}
