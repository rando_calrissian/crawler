package org.dudesoft.store;

import org.dudesoft.model.CrawlData;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public interface CrawlDataStore {

  /**
   * Serialize to file the Crawl data using the given name.
   * @param os OutputStream the crawl data should be written to.
   * @param data A CrawlData object to be serialized to the OutputStream.
   * @throws IOException if there was a problem writing to the provided output stream
   */
  void save(OutputStream os, CrawlData data) throws IOException;

  /**
   * Create a CrawlData object from the provided input stream.
   * @param is InputStream to read from.
   * @return A CrawlData object created from the InputStream.
   * @throws IOException if there is a problem reading from the provided input stream.
   */
  CrawlData read(InputStream is) throws IOException;
}
