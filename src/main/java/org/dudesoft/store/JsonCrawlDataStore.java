package org.dudesoft.store;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.dudesoft.model.CrawlData;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class JsonCrawlDataStore implements CrawlDataStore {

  ObjectMapper mapper;

  public JsonCrawlDataStore() {
    mapper = new ObjectMapper();
    mapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    mapper.setDateFormat(new StdDateFormat());
  }

  @Override
  public void save(OutputStream os, CrawlData data) throws IOException {
    mapper.writeValue(os, data);
  }

  @Override
  public CrawlData read(InputStream is) throws IOException {
    return mapper.readValue(is, CrawlData.class);
  }
}
