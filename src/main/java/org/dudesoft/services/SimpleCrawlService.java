package org.dudesoft.services;

import org.apache.commons.lang3.Validate;
import org.dudesoft.model.CrawlData;
import org.dudesoft.store.CrawlDataStore;
import org.eclipse.jetty.http.HttpStatus;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Date;

/**
 * Services to retrieve the contents of a URL and parse out relevant data.
 */
public class SimpleCrawlService implements Crawler {

  private static final Logger log = LoggerFactory.getLogger(SimpleCrawlService.class);

  private CrawlDataStore dataStore;

  public SimpleCrawlService(CrawlDataStore dataStore) {
    Validate.notNull(dataStore);
    this.dataStore = dataStore;
  }

  @Override
  public CrawlData retrieve(String url)  {
    CrawlData data = new CrawlData(url, new Date());

    log.info("Crawl url {}", url);

    // make the http request
    Connection connection = Jsoup.connect(url);
    Connection.Response response;
    try {
      response = connection.execute();
    } catch (IOException ioe) {
      throw new CrawlException("Unable to execute HTTP request", ioe);
    }

    int statusCode = response.statusCode();
    if (statusCode < HttpStatus.OK_200 || statusCode >= HttpStatus.MULTIPLE_CHOICES_300) {
      throw new HttpCrawlException(statusCode, response.statusMessage());
    }

    data.setHeaders(response.headers());

    // parse the HTTP response body as HTML
    Document document = Jsoup.parse(response.body());
    data.setTitle(document.title());
    data.setBody(response.body());

    return data;
  }

  @Override
  public void downloadToDataFile(String url, String path) {
    // create data object from URL.
    CrawlData data = retrieve(url);

    log.info("Create data file at {}", path);
    try (OutputStream os = new FileOutputStream(path)) {
      dataStore.save(os, data);
    } catch (IOException e) {
      throw new CrawlException("Unable to create raw data file from URL.", e);
    }
  }

  @Override
  public CrawlData readFromFile(String path) {
    log.debug("Read data file from {}", path);

    try (InputStream is = new FileInputStream(path)) {
      return dataStore.read(is);
    } catch (IOException e) {
      throw new CrawlException("Unable to create CrawlData from file path: " + path, e);
    }
  }
}