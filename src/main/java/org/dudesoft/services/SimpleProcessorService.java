package org.dudesoft.services;

import org.jsoup.Jsoup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SimpleProcessorService implements Processor {

  private static final Logger log = LoggerFactory.getLogger(SimpleProcessorService.class);

  private static final Pattern EMAIL_PATTERN = Pattern.compile("[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\\.[a-zA-Z0-9-.]+");
  private static final Pattern URL_PATTERN = Pattern.compile("(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
          + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
          + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
      Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);

  @Override
  public List<String> extractEmails(String body) {
    List<String> emails = new ArrayList<>();
    Matcher matcher = EMAIL_PATTERN.matcher(body);
    while (matcher.find()) {
      emails.add(matcher.group());
    }
    return emails;
  }

  @Override
  public List<String> extractExternalLinks(String body, String host) {
    log.info("Find links in body external to host {}", host);
    List<String> links = new ArrayList<>();
    Matcher matcher = URL_PATTERN.matcher(body);
    // remove www from host if there
    host = removeWww(host);

    while (matcher.find()) {
      String link = matcher.group().trim();
      try {
        // parse URL and compare domain name
        URL url = new URL(link);

        // remove www from urlHost if there
        String urlHost = url.getHost();
        urlHost = removeWww(urlHost);

        // we only want external hosts, so only include cases where the host names do not match
        if (!urlHost.equalsIgnoreCase(host)) {
          links.add(matcher.group().trim());
        }
      } catch (MalformedURLException e) {
        // if it's not a valid URL we're not interested in it
      }

    }
    log.info("Found {} external links.", links.size());
    return links;
  }

  @Override
  public String removeHtmlTags(String body) {
    return Jsoup.parse(body).text();
  }

  private String removeWww(String host) {
    if (host.toLowerCase().indexOf("www.") == 0) {
      host = host.substring(4);
    }
    return host;
  }
}
