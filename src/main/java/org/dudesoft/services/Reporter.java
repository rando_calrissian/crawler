package org.dudesoft.services;

import org.dudesoft.model.CrawlData;
import org.dudesoft.model.Report;

import java.util.List;
import java.util.Optional;

public interface Reporter {

  /**
   * Build a {@link Report} from the provided {@link CrawlData}. If terms are provided, the Report will contain the
   * number of occurrences of each search term fond in the body.
   * @param data The crawl data used as the source for the Report.
   * @param terms Optional list of terms to search for in the body of the CrawlData.
   * @return A new Report object created from the CrawlData.
   */
  Report buildReport(CrawlData data, Optional<List<String>> terms);
}
