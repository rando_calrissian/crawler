package org.dudesoft.services;

public class CrawlException extends RuntimeException {

  public CrawlException(String message, Exception cause) {
    super(message, cause);
  }
}
