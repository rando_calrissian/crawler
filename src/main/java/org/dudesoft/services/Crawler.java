package org.dudesoft.services;

import org.dudesoft.model.CrawlData;

public interface Crawler {

  /**
   * Fetches data from the supplied URL, storing the notable data in a {@link CrawlData} object.
   * @param url The URL to crawl.
   * @return A CrawlData object representing content of the URL.
   */
  CrawlData retrieve(String url);

  /**
   * Given the provided URL, fetch the contents of the response and write to a data file.
   * @param url The URL to crawl.
   * @param filePath The path to write the crawl data file to.
   */
  void downloadToDataFile(String url, String filePath);

  /**
   * Creates a {@link CrawlData} object from a file specified by the path argument.
   * @param path Path to the file.
   * @return A {@link CrawlData} object created from the file represented by the path argument.
   */
  CrawlData readFromFile(String path);
}
