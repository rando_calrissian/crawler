package org.dudesoft.services;

import org.apache.commons.lang3.Validate;
import org.dudesoft.model.CrawlData;
import org.dudesoft.model.Report;
import org.dudesoft.search.InMemorySearchIndexer;
import org.dudesoft.search.SearchIndexer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

public class ReportService implements Reporter {

  private static final Logger log = LoggerFactory.getLogger(ReportService.class);

  private Processor processor;
  private SearchIndexer searchIndexer;

  public ReportService(Processor processor) {
    Validate.notNull(processor);
    this.processor = processor;
    this.searchIndexer = new InMemorySearchIndexer();
  }

  @Override
  public Report buildReport(CrawlData data, Optional<List<String>> terms) {
    Report report = new Report(data.getUrl(), data.getCrawlDate());
    report.setTitle(data.getTitle());
    report.setHeaders(data.getHeaders());

    // extract emails
    report.setEmails(processor.extractEmails(data.getBody()));

    // extract links
    try {
      URL parsedURL = new URL(report.getUrl());
      report.setOutlinks(processor.extractExternalLinks(data.getBody(), parsedURL.getHost()));
    } catch (MalformedURLException e) {
      log.error("Could not parse URL.", e);
    }

    // strip HTML tags
    report.setBody(processor.removeHtmlTags(data.getBody()));

    // count search term occurrences
    List<Map<String, Integer>> custom = null;
    if (terms.isPresent()) {
      // if there exists at least 1 search term, build the index
      log.info("indexing crawl data body");
      searchIndexer.addDocument(data.getBody());
      custom = terms.get()
          .stream()
          .map(getCountTermOccurrencesFunction())
          .collect(Collectors.toList());
    }
    report.setCustom(custom);


    return report;
  }

  /**
   * Returns a {@link Function} that accepts a search term, and returns {@link Map} containing the search term as the
   * key, and the number of occurrences of that search term as the value.
   * @return A Function that will count search occurrences in the search index for the provided search term.
   */
  protected Function<String, Map<String, Integer>> getCountTermOccurrencesFunction() {
    return (term) -> {
      long occurrences;
      try {
        occurrences = searchIndexer.search(term);
      } catch (Exception e) {
        log.error("Unable to count occurrences of {} in document.", term, e);
        throw new RuntimeException(e);
      }
      Map<String, Integer> searchResult = new HashMap<>();
      searchResult.put(term, (int) occurrences);
      return searchResult;
    };
  }
}
