package org.dudesoft.services;

import java.util.List;

public interface Processor {

  /**
   * Create a {@link List} of email addresses extracted the provided string.
   * @param s The string to search for and extract email addresses from.
   * @return a {@link List} of email addresses extracted the provided string.
   */
  List<String> extractEmails(String s);

  /**
   * Create a {@link List} of URL strings extracted from the provided string. Only URLs will be returned which do not
   * match the provided host.
   * @param s The string to search for and extract links addresses from.
   * @return a {@link List} of external links extracted the provided string.
   */
  List<String> extractExternalLinks(String s, String host);

  /**
   * Given the supplied String, remove any HTML tags.
   * @param body An arbitrary String of data, that may or may not contain HTML tags.
   * @return The supplied string but with HTML tags removed.
   */
  String removeHtmlTags(String body);
}
