package org.dudesoft.services;

public class HttpCrawlException extends RuntimeException {

  private int statusCode;

  public HttpCrawlException(int statusCode, String message) {
      super(message);
      this.statusCode = statusCode;
  }
}
