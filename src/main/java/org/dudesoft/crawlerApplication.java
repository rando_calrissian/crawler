package org.dudesoft;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.dudesoft.cli.CrawlCommand;

public class crawlerApplication extends Application<crawlerConfiguration> {

    public static void main(final String[] args) throws Exception {
        new crawlerApplication().run(args);
    }

    @Override
    public String getName() {
        return "crawler";
    }

    @Override
    public void initialize(final Bootstrap<crawlerConfiguration> bootstrap) {
        bootstrap.addCommand(new CrawlCommand());
    }

    @Override
    public void run(final crawlerConfiguration configuration, final Environment environment) {
    }

}
