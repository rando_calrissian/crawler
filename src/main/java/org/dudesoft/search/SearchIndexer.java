package org.dudesoft.search;

public interface SearchIndexer {

  void addDocument(String content);

  long search(String term) throws Exception;
}
