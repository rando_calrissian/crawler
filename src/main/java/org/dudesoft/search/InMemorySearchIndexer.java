package org.dudesoft.search;

import org.apache.commons.lang3.Validate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

public class InMemorySearchIndexer implements SearchIndexer {

  private static final Logger log = LoggerFactory.getLogger(InMemorySearchIndexer.class);

  String[] tokens = {};

  @Override
  public void addDocument(String content) {
    Validate.notEmpty(content);

    tokens = content.replaceAll("[^a-z\\sA-Z]","").split(" ");
  }

  @Override
  public long search(String term) throws Exception {
    Validate.notEmpty(term);

    // Build a Query object
    return Arrays.stream(tokens)
        .filter((token) -> term.equalsIgnoreCase(token))
        .count();
  }
}
