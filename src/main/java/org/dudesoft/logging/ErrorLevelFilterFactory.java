package org.dudesoft.logging;

import ch.qos.logback.classic.Level;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.filter.Filter;
import ch.qos.logback.core.spi.FilterReply;
import com.fasterxml.jackson.annotation.JsonTypeName;
import io.dropwizard.logging.filter.FilterFactory;

@JsonTypeName("error-level-filter-factory")
public class ErrorLevelFilterFactory implements FilterFactory<ILoggingEvent> {

  @Override
  public Filter<ILoggingEvent> build() {
    return new Filter<ILoggingEvent>() {

      @Override
      public FilterReply decide(ILoggingEvent iLoggingEvent) {
        if (iLoggingEvent.getLevel().isGreaterOrEqual(Level.WARN)) {
          return FilterReply.ACCEPT;
        }
        return FilterReply.DENY;
      }
    };
  }
}
