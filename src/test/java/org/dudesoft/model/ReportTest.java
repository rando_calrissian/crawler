package org.dudesoft.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

public class ReportTest {

  private ObjectMapper objectMapper;

  @Before
  public void setUp() {
    objectMapper = new ObjectMapper();
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    objectMapper.setDateFormat(new StdDateFormat());
  }

  @Test
  public void testConstructor() throws ParseException, JsonProcessingException {
    // Build a Java Date object from a string so it can be compared to the resulting JSON ISO 8601 value
    String dateString = "1977-05-25T12:00:00.000+0000";
    DateTime dateTime = new DateTime(dateString);

    Report report = new Report("myurl", dateTime.toDate());

    String json = objectMapper.writeValueAsString(report);
    assertThat(json, containsString(dateString));
    assertThat(json, containsString("\"url\":\"myurl\""));
  }

  @Test
  public void testCustomFields() throws JsonProcessingException {

    // build some custom "search hit" objects
    List<Map<String, Integer>> customs = new ArrayList<>();
    Map<String, Integer> custom = new HashMap<>();
    custom.put("email", 1);
    customs.add(custom);

    custom = new HashMap<>();
    custom.put("title", 0);
    customs.add(custom);

    custom = new HashMap<>();
    custom.put("page", 1);
    customs.add(custom);

    // create Report object and add custom fields
    Report report = new Report("myurl", new Date());
    report.setCustom(customs);

    // convert to json
    String json = objectMapper.writeValueAsString(report);
    assertThat(json, containsString("\"custom\":[{\"email\":1},{\"title\":0},{\"page\":1}]"));
  }
}
