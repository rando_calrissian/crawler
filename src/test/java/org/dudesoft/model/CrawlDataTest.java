package org.dudesoft.model;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Test;

import java.text.ParseException;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class CrawlDataTest {

  private ObjectMapper objectMapper;

  @Before
  public void setUp() {
    objectMapper = new ObjectMapper();
    objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    objectMapper.setDateFormat(new StdDateFormat());
  }

  @Test
  public void testConstructor() throws ParseException, JsonProcessingException {
    // Build a Java Date object from a string so it can be compared to the resulting JSON ISO 8601 value
    String dateString = "1977-05-25T12:00:00.000+0000";
    DateTime dateTime = new DateTime(dateString);

    CrawlData data = new CrawlData("myurl", dateTime.toDate());

    String json = objectMapper.writeValueAsString(data);
    assertThat(json, containsString(dateString));
    assertThat(json, containsString("\"url\":\"myurl\""));
  }

}
