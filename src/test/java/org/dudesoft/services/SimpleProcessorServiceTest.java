package org.dudesoft.services;

import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class SimpleProcessorServiceTest {

  Processor processor = new SimpleProcessorService();

  @Test
  public void testExtractEmails() {
    String s = "asdf sfwe @  fjejej. lddl --  joe # joe.com az@111cc.biz <hello mailto=\"hi@hello.org\">";
    List<String> emails = processor.extractEmails(s);

    assertEquals(2, emails.size());
    assertEquals("az@111cc.biz", emails.get(0));
    assertEquals("hi@hello.org", emails.get(1));
  }

  @Test
  public void testExtractEmailsEmpty() {
    String s = "asdf sfwe @  fjejej. lddl --  joe # joe.com az@ 111cc.biz <hello mailto=\"hi@h++ello.org\">";
    List<String> emails = processor.extractEmails(s);

    assertEquals(0, emails.size());
  }

  @Test
  public void testExtractExternalLinks() {
    String s = "asdf sfwe @  fyjejej. ftp://ass.dkw.biz/asdf/we lddl --  joe # joe.com az@111cc.biz <hello mailto=\"hi@hello.org\">\n\nhttp ssswwi https:///// wef https://test.com/index.html ddw3 4 4((@ ";
    List<String> links = processor.extractExternalLinks(s, "ass.dkw.biz");

    assertEquals(1, links.size());
    assertEquals("https://test.com/index.html", links.get(0));
  }

  @Test
  public void testExtractExternalLinksEmpty() {
    String s = "asdf sfwe @  fjejej. ftpw://ass.dkw.biz/asdf/we lddl --  joe # joe.com az@111cc.biz <hello mailto=\"hi@hello.org\">\n\nhttp ssswwi https:///// wef https://te%st.com/index.html ddw3 4 4((@ ";
    List<String> links = processor.extractExternalLinks(s, "ass.dkw.biz");

    assertEquals(0, links.size());
  }

  @Test
  public void testExtractExternalLinksWww() {
    String s = "asdf sfwe @  fyjejej. ftp://ass.dkw.biz/asdf/we lddl --  joe # joe.com az@111cc.biz <hello mailto=\"hi@hello.org\">\n\nhttp ssswwi https:///// wef https://test.com/index.html ddw3 4 4((@ ";
    List<String> links = processor.extractExternalLinks(s, "www.test.com");

    assertEquals(1, links.size());
    assertEquals("ftp://ass.dkw.biz/asdf/we", links.get(0));
  }

  @Test
  public void testStripHtmlTags() {
    String html = "This is some <b>text</b> with </br> some random <div><span>tags</span></div> all up in it.</body>";

    String result = processor.removeHtmlTags(html);
    assertEquals("This is some text with some random tags all up in it.", result);
  }

  @Test
  public void testStripHtmlTagsWhenNonePresent() {
    String html = "This is some text without any markup. < 5";

    String result = processor.removeHtmlTags(html);
    assertEquals(html, result);
  }
}
