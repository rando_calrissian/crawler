package org.dudesoft.services;

import org.dudesoft.search.InMemorySearchIndexer;
import org.dudesoft.search.SearchIndexer;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InMemorySearchIndexerTest {

  private SearchIndexer searchIndexer = new InMemorySearchIndexer();

  @Test
  public void testSearch() throws Exception {
    searchIndexer.addDocument("Friends, romans, country men. Now is the time for all good men to come to the aid of their country.");
    assertEquals(1, searchIndexer.search("good"));
    assertEquals(0, searchIndexer.search("hello"));
    assertEquals(2, searchIndexer.search("to"));
    assertEquals(2, searchIndexer.search("country"));

  }
}
