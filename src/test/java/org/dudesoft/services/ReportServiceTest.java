package org.dudesoft.services;

import org.dudesoft.model.CrawlData;
import org.dudesoft.model.Report;
import org.junit.Before;
import org.junit.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ReportServiceTest {

  Processor processor;
  Reporter reporter;

  @Before
  public void setup() {
    processor = mock(Processor.class);
    reporter = new ReportService(processor);
  }

  @Test
  public void testBuildReport() throws MalformedURLException {
    final String host = "https://myhost.org/index.php";
    final Date crawlDate = new Date();
    final String title = "Hello whirled";
    final Map<String, String> headers = new TreeMap<>();
    headers.put("Content-Type", "text/html");
    headers.put("Content-Length", "323123");
    final String body = "<html><head><title>asdfsad</title><link>http://www.test.com</link></head><body>this is the body <a href=\"mailto:joe@joe.com\">joe</a></body></html>";

    List<String> emails = Arrays.asList(new String[] { "asdf@asdf.org", "qwerty@qwerty.edu" });
    when(processor.extractEmails(body)).thenReturn(emails);

    List<String> links = Arrays.asList( new String[] { "http://hello.gov" });
    URL parsedURL = new URL(host);
    when(processor.extractExternalLinks(body, parsedURL.getHost())).thenReturn(links);

    when(processor.removeHtmlTags(body)).thenReturn("no tags here");

    // finally set up the CrawlData instance and build the report
    CrawlData data = new CrawlData(host, crawlDate);
    data.setHeaders(headers);
    data.setTitle(title);
    data.setBody(body);

    Report report = reporter.buildReport(data, Optional.empty());

    assertEquals(host, report.getUrl());
    assertEquals(crawlDate.getTime(), report.getCrawlDate().getTime());
    assertEquals(title, report.getTitle());
    assertEquals(headers, report.getHeaders());
    assertEquals("no tags here", report.getBody());
    assertEquals(emails, report.getEmails());
    assertEquals(links, report.getOutlinks());
  }
}
