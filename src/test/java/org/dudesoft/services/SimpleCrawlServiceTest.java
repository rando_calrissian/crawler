package org.dudesoft.services;

import org.dudesoft.model.CrawlData;
import org.dudesoft.store.CrawlDataStore;
import org.dudesoft.store.JsonCrawlDataStore;
import org.eclipse.jetty.http.HttpStatus;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import static org.eclipse.jetty.http.HttpHeader.CACHE_CONTROL;
import static org.eclipse.jetty.http.HttpHeader.CONTENT_TYPE;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.powermock.api.mockito.PowerMockito.mock;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ Jsoup.class })
public class SimpleCrawlServiceTest {

  private CrawlDataStore dataStore = new JsonCrawlDataStore();

  @Test(expected = CrawlException.class)
  public void testRetrieveIOException() throws IOException {
    String url = "http://myurl.com";

    mockStatic(Jsoup.class);
    Connection con = mock(Connection.class);
    when(con.execute()).thenThrow(new IOException("help"));
    when(Jsoup.connect("http://myurl.com")).thenReturn(con);

    Crawler crawler = new SimpleCrawlService(dataStore);
    crawler.retrieve(url);
  }

  @Test(expected = HttpCrawlException.class)
  public void testRetrieveNon200Status() throws IOException {
    String url = "http://myurl.com";

    mockStatic(Jsoup.class);
    Connection con = mock(Connection.class);
    Connection.Response response = mock(Connection.Response.class);
    when(con.execute()).thenReturn(response);
    when(response.statusCode()).thenReturn(HttpStatus.NOT_FOUND_404);
    when(Jsoup.connect("http://myurl.com")).thenReturn(con);

    Crawler crawler = new SimpleCrawlService(dataStore);
    crawler.retrieve(url);
  }

  @Test
  public void testRetrieve200Ok() throws IOException {
    String url = "http://myurl.com";

    mockStatic(Jsoup.class);
    Connection con = mock(Connection.class);
    Connection.Response response = mock(Connection.Response.class);

    when(con.execute()).thenReturn(response);
    when(response.statusCode()).thenReturn(HttpStatus.OK_200);

    // headers
    Map<String, String> headers = new TreeMap<>();
    headers.put(CONTENT_TYPE.toString(), "text/html");
    headers.put(CACHE_CONTROL.toString(), "None");
    when(response.headers()).thenReturn(headers);

    Document doc = mock(Document.class);
    when(Jsoup.parse(anyString())).thenReturn(doc);
    when(doc.title()).thenReturn("My Title!");
    when(response.body()).thenReturn("<body>The doc body</body>");
    when(Jsoup.connect("http://myurl.com")).thenReturn(con);

    Crawler crawler = new SimpleCrawlService(dataStore);
    CrawlData data = crawler.retrieve(url);

    assertEquals("My Title!", data.getTitle());
    assertEquals("<body>The doc body</body>", data.getBody());
  }
}
