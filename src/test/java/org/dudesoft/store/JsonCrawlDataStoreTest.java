package org.dudesoft.store;

import org.dudesoft.model.CrawlData;
import org.joda.time.DateTime;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import static org.junit.Assert.assertEquals;

public class JsonCrawlDataStoreTest {

  @Test
  public void testSave() throws IOException {
    CrawlDataStore dataStore = new JsonCrawlDataStore();
    String dateString = "1977-05-25T12:00:00.000+0000";
    DateTime dateTime = new DateTime(dateString);

    CrawlData data = new CrawlData("myurl", dateTime.toDate());
    data.setBody("<body>This is the body</body>");
    data.setTitle("My Document!");
    String json = null;

    try (OutputStream os = new ByteArrayOutputStream()) {
      dataStore.save(os, data);
      json = os.toString();
    }

    assertEquals("{\"url\":\"myurl\",\"crawlDate\":\"1977-05-25T12:00:00.000+0000\",\"headers\":null,\"title\":\"My Document!\",\"body\":\"<body>This is the body</body>\"}",
        json);

  }
}
